Combinar documents XML amb XInclude
===================================

MP4UF1A1T5

“Transclusió” de documents XML amb XInclude

XInclude
--------

-   Els documents inclosos amb entitats generals externes, entre altres
    dificultats, no són validables. Amb XInclude aquest
    problema desapareix.
-   Per fer servir XInclude cal declarar el seu espai de noms, en forma
    d’un atribut especial: `xmlns:xi="http://www.w3.org/2001/XInclude"`.
    Això es pot fer en qualsevol element, encara que és habitual fer-ho
    en l’element arrel.
-   L’element `xi:include` permet incloure altres fitxers:
    `<xi:include href="URL"/>`.

L’ordre `xmllint`(1) por ser usada per expandir documents que facin
servir XInclude.

Enllaços recomanats
-------------------

-   [Wikipedia: Transclusion](http://en.wikipedia.org/wiki/Transclusion)
-   [XInclude
    Tutorial](http://www.zvon.org/xxl/XIncludeTutorial/Output/contents.html)
-   [Using XInclude](http://www.xml.com/pub/a/2002/07/31/xinclude.html)

Pràctiques
----------

-   Practica els tutorials citats en la llista d’enllaços.
-   Prenent com a base el catàleg de CDs del tutorial d’XML, prepara un
    document XML que utilitzi XInclude per combinar diferents
    fitxers XML.

