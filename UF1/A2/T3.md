Organitzar i documentar DTDs
============================

MP4UF1A2T3

Tècniques i normes de documentació del DTDs

Entitats de paràmetre internes i externes
-----------------------------------------

Les entitats de paràmetre ofereixen un simple sistema de substitució de
text a emprar dins del DTD.

-   Es defineixen com les generals però afegint el caràcter `%`
    (`<!ENTITY % …>`) .
-   Es defineixen i referèncien sempre dins del DTD, mai en el document.
-   Es deferèncien amb la sintaxi `%NOM;`.
-   Es poden fer servir per dividir un gran DTD en diferents fitxers
    (fent servir entitats externes).

Comentaris
----------

-   Capçalera: autor, descripció del contingut del document, registre de
    canvis…
-   Seccions principals: separen blocs importants del DTD (no es
    recomana l’estil *banner*).
-   Element: per a cada element cal una descripció, sense oblidar els
    seus atributs.

Enllaços recomanats
-------------------

-   [XML
    Avanzado](http://www.di.uniovi.es/~labra/cursos/XMLAvanzado/XML.html)
    ([local](aux/XMLAvanzado/XML.html))
-   [XML Syntax Quick
    Reference](http://www.mulberrytech.com/quickref/XMLquickref.pdf)
    ([local](../A1/aux/XMLquickref.pdf))

Pràctiques
----------

-   Crea (i expandeix amb `xmllint`(1) –busca les opcions necessàries en
    la documentació de l’ordre–) un document XML que inclogui
    (utilitzant entitats de paràmetre externes) les declaracions
    d’entitat que acompanyen el DTD de l’XHTML (en els fitxers amb
    extensió `.ent`). Utilitza algunes d’aquestes entitats dins del text
    del document i verifica que es poden expandir correctament.
-   Documenta el document i DTD de menú de restaurant que has fet en una
    de les primeres pràctiques d’aquesta activitat.

